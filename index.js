const yargs = require("yargs");
const execa = require("execa");
const mkdirp = require("mkdirp");

async function initHandler(_argv) {
    await mkdirp("content");

    function executeThis(command) {
        return execa(process.argv[0], [process.argv[1], ...command], { stdio: "inherit" });   
    }

    const commandsToExecute = [
        ["downloadList"],
        ["downloadFiles"],
        ["html2md"],
        ["versionDate"],
        ["analyzeFassungen"],
        ["git"]
    ];

    for (i in commandsToExecute) {
        const command = commandsToExecute[i];
        console.log(`Step ${(i|0)+1} of ${commandsToExecute.length}: ${command}`);
        await executeThis(command);
    }
}

yargs
  .commandDir("./commands")
  .command("init", "Convenience command to execute all commands needed the first time", {}, initHandler)
  .help().demandCommand().strict()
  .argv;