const fs = require("fs").promises;
const mkdirp = require("mkdirp");
const domino = require("domino");
const TurndownService = require('turndown');

const turndownService = new TurndownService();
turndownService.addRule('remove_einzelnorm', {
    filter: (node, options) => {
        return node.nodeName === "P" && node.classList.contains("einzelnorm");
    },
    replacement: function() { return ''; }
});
turndownService.addRule('paragraph_ueberschrift', {
    filter: (node, options) => {
        return (node.nodeName === "H4" || node.nodeName === "H3") && node.classList.contains("paragraph_bez");
    },
    replacement: (content, node, options) => {
        let result = node.nodeName === "H4" ? "#### " : "### ";
        for (ele of node.childNodes) {
            if (ele.nodeName === "#text")
                result += ele.wholeText;
            else if (ele.nodeName === "SPAN") {
                result += " " + ele.innerHTML;
        }
        }
        return result;
    }
})
turndownService.addRule('dot', {
    filter: (node, options) => {
        return node.nodeName === 'SPAN' && node.className === "internal-dot";
    },
    replacement: (content, node, options) => {
        return ".\n";
    }
})


function traverseAndInsertNewlines(document, node) {
    for (n of node.childNodes) {
        if (n.nodeType == 3) {
            const sentences = n.nodeValue.split(/\.[ \n\t$]/g);

            if (sentences.length === 1) {
                continue;
            }

            let resultList = [];
            for (sentence of sentences) {
                if (sentence !== '') {
                    resultList.push(document.createTextNode(sentence));
                }
                const dot = document.createElement("SPAN");
                dot.className = "internal-dot";
                dot.insertAdjacentText('afterbegin', '.');
                resultList.push(dot);
            }

            resultList.pop();

            for (l of resultList) {
                node.insertBefore(l, n);
            }

            node.removeChild(n);
        } else {
            traverseAndInsertNewlines(document, n);
        }
    }
}

// The links sometimes contain numbers, that change during different runs,
// (a bit similarly to the laws themselves, so that's probably because attachments are also versioned).
// These numbers occasionally change (making the earlier ones not reachable anymore).
// We don't want this instability, so we remove them here.
// This also generates dead links, but at least they're stable.
// (For even better coverage, we'd probably need to also download these attachments
//  and link them internally somehow).
const linkRegex = /^(\/br2\/sixcms\/media.php.*\.)[0-9]*\.(.*)$/;
const attributesWithLinksToBeReplaced = {
    A: "href",
    IMG: "src"
};
function changeLinksToAttachmentsAndImages(node) {
    for (n of node.childNodes) {
        const attributeName = attributesWithLinksToBeReplaced[n.nodeName];
        if (attributeName) {
            const link = n.getAttribute(attributeName);
            if (link && link.startsWith("/br2/sixcms/media.php")) {
                const regexResult = linkRegex.exec(link);
                if (regexResult) {
                    const newLink = regexResult[1] + regexResult[2];
                    console.log(`changing link from ${link} to ${newLink} in ${attributeName}`);
                    n.setAttribute(attributeName, newLink);
                }
            }
        }

        changeLinksToAttachmentsAndImages(n);
    }
}

function convert(content, file) {
    const document = domino.createDocument(content);

    const normTitle = document.querySelector("#norm_title");
    let title = "";
    if (normTitle) title = "## " + normTitle.textContent + "\n\n";
    if (!normTitle)
        console.error(`No title in ${file}`);

    let relevant = document.querySelector(".reiterbox_innen_text");
    if (relevant === undefined) {
        console.error(`Invalid structure in ${file}`);
        relevant = "<p>Error while converting</p>"
    } else {
        traverseAndInsertNewlines(document, relevant);
        changeLinksToAttachmentsAndImages(relevant);
    }
    return title + turndownService.turndown(relevant);
}

async function main() {
    const files = await fs.readdir("content/files");
    await mkdirp("content/generated/md");

    let i = 0;
    let skipped = 0;
    for (file of files) {
        if (await fs.stat("content/generated/md/" + file + ".md").catch(_reason => false)) {
            ++skipped;
        } else {
            const content = convert(await fs.readFile("content/files/" + file, "utf8"), file);
            await fs.writeFile("content/generated/md/" + file + ".md", content, "utf8");
        }
        ++i;
        if (i%100 == 0)
            console.log(`${i}/${files.length} (skipped: ${skipped})`);
    }
}

exports.command = "html2md";
exports.desc = "Transform laws from html to md. This reads laws from content/files/ and generates to content/generated/md/"
exports.handler = main;
