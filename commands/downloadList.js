const fs = require("fs").promises;
const {parse} = require("node-html-parser");


const overrides = {
    "/verordnungen/3_sars_cov_2_eindv": {
        original: [{
            "fassung": "Am 18. M&auml;rz 2022 ",
            "link": "/verordnungen/3_sars_cov_2_eindv",
            "fund": "GVBl.II/22, [Nr. 20]",
            "fundLink": "/br2/sixcms/media.php/76/GVBl_II_20_2022.pdf"
        }],
        corrected: [{
            "fassung": "Ursprüngliche Fassung vom vom 22. Februar 2022 ",
            "link": "/verordnungen/3_sars_cov_2_eindv",
            "fund": "GVBl.II/22, [Nr. 20]",
            "fundLink": "/br2/sixcms/media.php/76/GVBl_II_20_2022.pdf"
        }, {
            "fassung": "Außer Kraft getreten durch Verordnung vom 17. M&auml;rz 2022",
            "link": "/verordnungen/3_sars_cov_2_eindv",
            "fund": "GVBl.II/22, [Nr. 28]",
            "fundLink": "/br2/sixcms/media.php/76/GVBl_II_28_2022.pdf"
        }]
    }
};

function overrideLaw(law, result) {
    const override = overrides[law];
    if (!override) {
        return result;
    }
    if (JSON.stringify(override.original) === JSON.stringify(result)) {
        return override.corrected;
    } else {
        console.warn(`Override for ${law} doesn't apply, because original differs now: ${JSON.stringify(result)}`);
        return result;
    }
}


const got = require("got").extend({
    prefixUrl: "https://bravors.brandenburg.de",
    headers: {
		'user-agent': `brandenburg-laws-to-git (https://gitlab.com/fkz/brandenburg-laws-to-git)`
	}
});

function parseSearchResult(body) {
    const dom = parse(body);
    const list = dom.querySelector(".chrono-list").querySelector(".ergebnisliste");
    const dtEles = list.childNodes.filter(value => value.rawTagName === "dt");

    return dtEles.map(dtEle => {
        const aAttribute = dtEle.querySelector(".link a");
        return {
            link: aAttribute.getAttribute("href"),
            name: aAttribute.getAttribute("title")
        }
    });
}

// Sometimes, the server replies with an empty response, but still 200 OK.
// Sometimes it even replies with a reponse that misses something
// Then we retry to see if that may help
// In that case, retry once
async function retryUntil(url, bodyTransformer) {
    let count = 0;

    function logError(reason) {
        console.warn(`Wrong response from ${url}: ${reason}`)
    }

    while (count < 5) {
        ++count;
        const reply = await got(url);
        if (reply.body === undefined) {
            logError("body is undefined")
        } else if (reply.body === "") {
            logError("body is empty")
        } else {
            try {
                const result = await bodyTransformer(reply.body);
                if (count >= 2) {
                    console.log(`Succeeded at ${count}. try`)
                }
                return result;
            } catch (e) {
                logError(e.message);
                console.log(e.stack);
            }
        }
        const sleepMs = (Math.random() * 15000) | 0;
        console.log(`Retrying in ${sleepMs}ms`)
        await sleep(sleepMs);
    }

    logError("Errors persisted after 5 retries");
    throw new Error("retryUntil failed");
}

async function retrieveList(startYear) {
    startYear |= 0;
    if (startYear < 1990) {
        throw new Error("startYear must be at least 1990");
    }
    const currentYear = 2022;
    const length = currentYear - startYear + 1;
    const years = Array.from({length: length }, (_, index) => startYear + index);

    const archiveUrl = year => "de/archiv_fundstellennachweis_gesetzte_und_verordnungen_chronologisch/year/" + year;
    const activeUrl = year => "de/vorschriften_fundstellennachweis_gesetzte_und_verordnungen_chronologisch/year/" + year;

    let result = [];

    for (year of years) {
        console.log(year);
        const r1 = await retryUntil(archiveUrl(year), parseSearchResult);
        const r2 = await retryUntil(activeUrl(year), parseSearchResult);

        result = result.concat(r1, r2);
    }

    return result;
}

async function downloadLaw(law) {
    const historyUrl = law.link.substring(1) + "?history";

    const columns = await retryUntil(historyUrl, body => {
        const dom = parse(body);
        const historyTable = dom.querySelector("table.historie tbody");
        return historyTable.querySelectorAll("tr");
    });
    const result = columns.map(column => {
        const rows = column.querySelectorAll("td");
        let fassung = undefined;
        let fund;
        if (rows[0]) {
            fassung = rows[0].querySelector("a");
        }
        if (rows[1]) {
            fund = rows[1].querySelector("a");
            if (!fund) {
                console.log(`Fund is not a link in ${law.link}, using directly: "${rows[1]}"`);
                fund = rows[1];
            }
        }
        if (!fassung || !fund) {
            console.error(`Something is wrong with ${law.link}`);
            console.log(column.innerHTML);
            return { error: column.innerHTML };
        } else return {
            fassung: fassung.innerText,
            link: fassung.getAttribute("href"),
            fund: fund.innerText,
            fundLink: fund.getAttribute("href")
        }
    });

    if (overrides[law.link]) {
        return overrideLaw(law.link, result);
    } else {
        return result;
    }
}

function sleep(timeout) {
    return new Promise((resolve, _reject) => {
        setTimeout(() => resolve(), timeout);
    });
}

async function main(argv) {
    let before = {};
    if (argv.incremental) {
        const json = JSON.parse(await fs.readFile("content/list.json", "utf8"));
        for (ele of json) {
            before[ele.link] = ele;
        }
    }

    const list = await retrieveList(argv.startYear);
    let result = [];

    let i = 0;

    for (ele of list) {
        if (!argv.refetch && before[ele.link]) {
            result.push(before[ele.link])
        } else {
            i += 1;
            result.push({
                name: ele.name,
                link: ele.link,
                versions: await downloadLaw(ele)
            });
            if (i % 10 == 0) {
                console.log(`${i}/${list.length}, sleeping a bit`)
                await sleep(1000);
            }
        }
        delete before[ele.link];
    }

    console.log(`Downloaded ${i} histories`);

    for (ele of Object.values(before)) {
        result.push(ele);
    }

    result.sort((a, b) => {
        if (a.link < b.link)
            return -1;
        if (a.link > b.link)
            return 1;
        return 0;
    });

    await fs.writeFile("content/list.json", JSON.stringify(result, undefined, 2));

    console.log(`Wrote ${Object.keys(result).length} elements`)
}


exports.command = "downloadList";
exports.desc = "Generate list of laws to be downloaded in the next step.\n" +
               "The list is generated stored in content/list.json";
exports.builder = {
    incremental: {
        type: "boolean",
        default: false
    },
    refetch: {
        type: "boolean",
        default: false
    },
    startYear: {
        type: "number",
        default: 2010,
        description: "Year from which to start downloading laws; number between 1990 and 2021"
    }
};
exports.handler = main;