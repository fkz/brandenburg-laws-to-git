const fs = require("fs").promises;

async function main() {
    const fassungen = JSON.parse(await fs.readFile("content/generated/fassungen.json"));

    let fund = {};

    let result = [];

    let prevDate = fassungen[0].date;

    for (let fassung of fassungen) {
        if (fassung.date < prevDate) {
            console.error("wrong date order " + prevDate + ">" + fassung.date);
        }
        prevDate = fassung.date;
        let addNewEntry = true;
        if (fund[fassung.fund] !== undefined) {
            const previousDate = result[fund[fassung.fund]].date;
            const diff = new Date(fassung.date) - new Date(previousDate);
            const MS_PER_DAY = 1000 * 60 * 60 * 24;
            addNewEntry = diff > 15 * MS_PER_DAY;
        }
        if (addNewEntry) {
            fund[fassung.fund] = result.length;
            result.push({
                fund: fassung.fund,
                date: fassung.date,
                changes: []
            });
        }
        result[fund[fassung.fund]].changes.push({
            parentLink: fassung.parentLink,
            link: fassung.link,
            deleted: fassung.deleted        
        });
    }

    prevDate = result[0].date;
    for (let f of result) {
        if (f.date < prevDate) {
            console.error("wrong date order, something is wrong in this algorithm", prevDate, ">", f.date);
        }
        prevDate = f.date;
    }

    await fs.writeFile("content/generated/fassungen_combined.json", JSON.stringify(result, null, 2));
}

module.exports.command = "analyzeFassungen";
module.exports.desc = "Generate content/generated/fassungen_combined.json from content/generated/fassungen.json"
module.exports.builder = {};
module.exports.handler = main;
