const fs = require("fs").promises;

function findDate(line) {
    if (!line) return;
    
    const regex1 = /^Am (.*?) (\(.*\) )?(A|a)ußer Kraft (getreten|gesetzt) (\(Zeitablauf\) )?durch .*$/;
    const regex2 = /^Am (.*) aufgehoben durch .*$/;
    const regex3 = /^Außer Kraft getreten durch Verordnung vom (.*)$/;
    const regex4 = /^\(Diese Änderung wurde durch Artikel 3 des Gesetzes vom 19. Juni 2019 aufgehoben\.\) § 5 geändert durch Gesetz vom (.*)$/

    const match = line.match(regex1) || line.match(regex2) || line.match(regex3) || line.match(regex4);
    if (match) {
        return {
            date: match[1],
            deleted: true
        };
    }

    // Find dates in line
    const dateRegex = /[1-9][0-9]?\. (Januar|Februar|März|M\&auml;rz|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember) 20[0-2][0-9]/g;

    let count = 0;
    let result = undefined;
    for (let match of line.matchAll(dateRegex)) {
        result = match[0];
        ++count;
    }

    if (count == 0) {
        console.error(`No date found in ${line}`);
    } else if (count > 1) {
        console.error(`Multiple dates found in ${line}`);
    } else {
        return {
            date: result,
            deleted: false
        };
    }
}

function parseDate(line) {
    if(!line) return;
    const dateAndDeleted = findDate(line);
    const date = dateAndDeleted.date;

    const dateRegex = /^([1-9][0-9]?)\. (Januar|Februar|März|M\&auml;rz|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember) (20[0-2][0-9])$/;

    const monthMap = {
        Januar: "01",
        Februar: "02",
        März: "03",
        "M\&auml;rz": "03",
        April: "04",
        Mai: "05",
        Juni: "06",
        Juli: "07",
        August: "08",
        September: "09",
        Oktober: "10",
        November: "11",
        Dezember: "12"
    }

    const matches = date.match(dateRegex);
    if (!matches) console.log(line);
    let day = matches[1];
    const month = monthMap[matches[2]];
    if (!month) console.log(date);
    const year = matches[3];

    if (day.length == 1) day = "0" + day;

    return {
        date: `${year}-${month}-${day}`,
        deleted: dateAndDeleted.deleted
    };
}

async function main() {
    const list = JSON.parse(await fs.readFile("content/list.json"));
    let fassungen = list.flatMap(x => x.versions.map(y => {
        const p = parseDate(y.fassung);
        return {
            date: (p && p.date) || "0000-00-00",
            fund: y.fund,
            parentLink: x.link,
            link: y.link,
            deleted: p && p.deleted
        };
    }));

    fassungen.sort((x, y) => {
        if (x.date < y.date) {
            return -1;
        }
        if (x.date > y.date) {
            return 1;
        }
        return 0;
    });

    fs.writeFile("content/generated/fassungen.json", JSON.stringify(fassungen, undefined, 2));

    console.log(fassungen);
}

exports.command = "versionDate";
exports.desc = "Generate content/generated/fassungen.json from content/list.json"
exports.handler = main;
