const fs = require("fs").promises;
const mkdirp = require("mkdirp");

const got = require("got").extend({
    prefixUrl: "https://bravors.brandenburg.de",
    headers: {
		'user-agent': `brandenburg-laws-to-git (https://gitlab.com/fkz/brandenburg-laws-to-git)`
	}
});

const dir = "content/files";


async function downloadFile(url) {
    try {
        return await(got(url)).body
    } catch (e) {
        console.error("Error while trying to download", url);
        throw e;
    }
}

async function download(links) {
    await mkdirp(dir);

    const existingFiles = new Set(await fs.readdir(dir));
    const newFiles = new Set(links.map(link => {
        return link && link.replace(/\//g, "_2F_");
    }));

    const alreadyDownloaded = new Set(
        [...newFiles].filter(x => existingFiles.has(x))
    );

    const downloadedWithoutSource = new Set(
        [...existingFiles].filter(x => !newFiles.has(x))
    );

    const toBeDownloaded = new Set(
        [...newFiles].filter(x => !existingFiles.has(x))
    );

    if (downloadedWithoutSource.size > 0) {
        console.error(`There are ${downloadedWithoutSource.size} files with unknown source`);
    }

    const todo = toBeDownloaded.size;
    const done = alreadyDownloaded.size;
    const total = newFiles.size;

    console.log(`${total} files, ${done} already downloaded, ${todo} still to be done`);

    let i = 0;
    let percentage = 0;
    for (file of toBeDownloaded) {
        if (!file) {
            console.log("No file: " + file)
            continue;
        }

        const url = file.replace(/_2F_/g, "/");
        if (url.substring(0, 1) != "/") throw new Error("wrong format from url");
        const urlWithoutPrecedingSlash = url.substring(1);
        //TODO: make this more streaming
        //TODO: We should probably download to a temp location and then do an atomic move
        const body = await downloadFile(urlWithoutPrecedingSlash);
        fs.writeFile(dir + "/" + file, body, "utf8");

        ++i;
        const newPercentage = Math.floor(i / todo * 100);
        if (newPercentage > percentage) {
            percentage = newPercentage;
            console.log(`${newPercentage}% done`);
        }
    }
}


async function main() {
    const json = JSON.parse(await fs.readFile("content/list.json", "utf8"));
    const downloadLinks = json.flatMap(x => x.versions).map(x => x.link);

    download(downloadLinks)
}

module.exports.command = "downloadFiles";
module.exports.desc = "Reads content/list.json and downloads laws to content/files/"
module.exports.handler = main;