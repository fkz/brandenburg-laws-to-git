# About this repository

This repository contains code, that can download laws and executive orders from the official website of Brandenburg (https://bravors.brandenburg.de) and generate
a git repository out of it.

You can see how this result looks like in the generated repository here: [brandenburg-laws-in-git](https://gitlab.com/fkz/brandenburg-laws-in-git).

[![pipeline status](https://gitlab.com/fkz/brandenburg-laws-to-git/badges/master/pipeline.svg)](https://gitlab.com/fkz/brandenburg-laws-to-git/-/commits/master)

# How to run
## Prerequivites

You need to have `node` and `git` installed.
Before executing anything for the first time, `npm install` needs to be executed.

## Simple execution

To do everything needed, you can run a convenience command to download all the necessary files
and process them further by running:

```
node . init
```

In the end, you should have a `git` repository of laws at `content/generated/repo2`.

## Detailed execution

This convenience function will internally execute the following commands in order, that
can also be executed independently if only some of them need to be executed (e.g. for
more incremental builds).

### Download files

All files will be downloaded in the `content` folder, so you first need to do

```
mkdir content
```

### Generate list.json

You first need to download the list of laws by executing

```
node . downloadList
```

Note that you can parametrize this command with `--incremental` and `--refetch`.


The first time you execute this command, you set `incremental` to `false`, as otherwise the command tries to read some file that doesn't exist yet. The incrmental version may not alwas be perfect.

The output (and also input in case of incremental version) is stored in `content/list.json`.

Once the list is downloaded, next the actual laws need to be downloaded by executing.

By default, this only downloads laws that have been changed in the last 10 years.

### Download actual laws into content/files/ directory

Once `content/list.json` exist, the actual laws can be downloaded by executing

```
node . downloadFiles
```

This will download each law version into one file. Note, that this tries to only download new laws, but not all urls are always stable (esp the newest one usually doesn't have a version inside its url), so the incremental version doesn't always work correctly and you may need to delete some files to get the correct ones downloaded.

## Further process these files

Once these commands have been executed, all further commands don't connect with the internet anymore.

### Transform html->md

Execute 

```
node . html2md
```

This transforms all the downloaded html files into md files. It simply translates files from `content/files` into `content/generated/md`. It tries to only translate files that weren't translated before. So if you delete one of the files in `content/files`, you should also delete the corresponding file in `content/generated/md`,s.t. it gets recreated once it's redownloaded.

### Generate content/generated/fassungen.json

This command will transform the `content/list.json` file and generate a differently structured `fassungen.json` out of it, that is ordered by time instead of laws, s.t. we can in the next step generate git commits out of that.
Just execute

```
node . versionDate
```

to generate it. Whenever you execute `downloadList.js`, this command should also be executed to updated this dependent file accordingly.

### Generate content/generated/fassungen_combined.json

Furthermore, execute

```
node . analyzeFassungen
```

This will further process `fassungen.json` and generate `fassungen_combined.json`.

### Generate the actual git repo

Out of the previously generated files, we can now create a git repository by executing

```
node . git
```

This will create a git repository at the `content/generated/repo2` path.
Note that you should delete this directory in case it exists before executing this command, as it doesn't support
to be executed incrementally (all earlier steps can still be executed incrementally).

Finally, the repository is generated.

# Motivation

This project was motivated by the want to easier see differences in the Corona Verordnungen, that were often released during the Covid-19 pandemic. In it's generality, it also works to see differences in other Verordnungen, laws and contracts published by the Brandenburg state government.
